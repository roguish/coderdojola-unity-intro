﻿using UnityEngine;
using System.Collections;

public class SpitBallMultiShot : BulletSuperclass {
	public GameObject bullet1;
	public GameObject bullet2;
	public GameObject bullet3;

	private Vector3 _forward1;
	private Vector3 _forward2;
	private Vector3 _forward3;

	void Start()
	{
		//The bullet will always move the direction it is facing, which was
		//	set when we spawned it in the Player class.
		//We move it forward by getting this forward vector(we'll consider the 
		//	character's forward to be to the right of the sprite), then adding it
		//	to the bullet's current position.
		_forward1 = bullet1.transform.right + new Vector3( 0, 0.3f, 0);
		_forward2 = bullet2.transform.right;
		_forward3 = bullet3.transform.right + new Vector3( 0, -0.3f, 0);

		_forward1.Normalize();
		_forward2.Normalize();
		_forward3.Normalize();

	}

	// Update is called once per frame
	void Update () {
		Vector2 nextPosition1 = bullet1.transform.position;
		Vector2 nextPosition2 = bullet2.transform.position;
		Vector2 nextPosition3 = bullet3.transform.position;

		nextPosition1.x += _forward1.x*bulletSpeed;
		nextPosition1.y += _forward1.y*bulletSpeed;

		nextPosition2.x += _forward2.x*bulletSpeed;
		nextPosition2.y += _forward2.y*bulletSpeed;

		nextPosition3.x += _forward3.x*bulletSpeed;
		nextPosition3.y += _forward3.y*bulletSpeed;

		//Finally, we assign the position to our next position.
		bullet1.transform.position = nextPosition1;
		bullet2.transform.position = nextPosition2;
		bullet3.transform.position = nextPosition3;

		//Make sure the bullet is still in the bounds of the game.
		bool inBounds = true;
		if( !GameManager.instance.InGameBounds(bullet1.transform.position) || 
		   !GameManager.instance.InGameBounds(bullet2.transform.position) ||
		   !GameManager.instance.InGameBounds(bullet3.transform.position) )
		{
			inBounds = false;
		}

		//if it isn't, delete it.
		if(!inBounds) {
			Destroy(this.gameObject);
		}
	}

}
