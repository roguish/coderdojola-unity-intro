using UnityEngine;
using System.Collections;

public class BulletSuperclass : MonoBehaviour 
{
	public float bulletSpeed;
	public int damageStrength;
	public float bulletFireDelaySeconds;

	public BulletSuperclass ()
	{
		// empty
	}
}

