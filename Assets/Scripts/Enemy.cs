﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
	//class variables:
	//	public variables are visible in the Unity inspector and additional classes
	//	put [HideInInspector] above a public variable if you don't want it to be visible
	//	private variables are hidden.
	public int health;
	public float enemySpeed;
	public bool pickRandomSpeed;
	public float randomMinSpeed;
	public float randomMaxSpeed;


	private Vector2 _directionVector;
	private Vector3 _playerPosition;
	private AudioSource _audioSource;
	private bool _isDying = false;

	// Use this for initialization
	void Start () {
		_audioSource = GetComponent<AudioSource>();

		//set the speed and direction of our obstacle
		if (pickRandomSpeed) {
			SetRandomSpeed ();
		}

		_directionVector = transform.position*-1;
		_directionVector.Normalize();

		_playerPosition = GameObject.Find( "Player" ).transform.position;
//		Debug.Log (_playerPosition);
//		transform.LookAt( -_playerPosition );
		transform.rotation *= Quaternion.FromToRotation( Vector3.right, new Vector3( _directionVector.x, _directionVector.y, 0 ) );
	}
	
	// Update is called once per frame
	void Update () {
//		transform.LookAt( _playerPosition );

		//To move the obstacle, we first get it's current position from the transform.
		Vector2 nextPosition = transform.position;
		//Then, we add the direction vector to that position.
		nextPosition.x += _directionVector.x*enemySpeed;
		nextPosition.y += _directionVector.y*enemySpeed;
		//Finally, we assign the position to the next position we have created.
		transform.position = nextPosition;

		//Make sure the bullet is still in the bounds of the game.
		bool inBounds = GameManager.instance.InGameBounds(transform.position);

		//if it isn't, delete it.
		if(!inBounds) {
			Destroy(this.gameObject);
		}
	}

	//This function is called by the Unity engine when this object overlaps another.
	void OnCollisionEnter2D(Collision2D collidedWith) {
		//Objects can be tagged in the Unity Inspector so that they can
		//	easily be checked in code. Here, we have tagged bullets with
		//	a bullet tag so that any time an obstacle runs in to one, we
		//	can check and efficiently know that the bullet is what we ran
		//	in to.
//		Debug.Log("enemy collided with " + collidedWith.gameObject.tag + ", parent tag: " + collidedWith.transform.parent.gameObject.tag );
		if (collidedWith.gameObject.tag == "Bullet") {
			StepBackwards();
			// get bullet damage strength
			int tDamage = 0;
			if( collidedWith.transform.parent.gameObject.GetComponent<SpitBallSingleShot>() != null )
			{
				tDamage = collidedWith.transform.parent.gameObject.GetComponent<SpitBallSingleShot>().damageStrength;
			}
			else if( collidedWith.transform.parent.gameObject.GetComponent<SpitBallMultiShot>() != null )
			{
				tDamage = collidedWith.transform.parent.gameObject.GetComponent<SpitBallMultiShot>().damageStrength;
			}
			Debug.Log (tDamage);
			//Destroy the bullet the obstacle collided with.
//			Destroy(collidedWith.gameObject);
			// setActive false instead so that the other bullet(s) can remain active. 
			// the bullet container will be destroyed when it leaves the scene. 
			collidedWith.gameObject.SetActive( false );
			//Lower health if it overlapped a bullet.
			StartCoroutine( DecreaseHealth( tDamage ) );
		}
	}

	//This function will randomly choose a speed for the obstacle.
	void SetRandomSpeed() {
		//Random.range is a pre-existing function that will pick a number somewhere between
		//	the numbers we give it. The function may also pick the lower or upper bound we
		//	give it. This is called beign inclusive.
		enemySpeed = Random.Range( randomMinSpeed, randomMaxSpeed );
	}

	//function that will check if the object dies when health decreases
	IEnumerator DecreaseHealth( int pDamage ) {
		health -= pDamage;
		if (health <= 0 && !_isDying) {
			// prevent multiple calls to this while the sprite is still active for audio purposes, but invisible. 
			_isDying = true;
			GetComponent<BoxCollider2D>().enabled = false;

			// increment score
			Model.instance.ChangeScore( 1 );
			GameManager.instance.UpdateScoreDisplay();

			// make invisible. Don't set active false b/c we still want to play sounds
			GetComponent<SpriteRenderer>().color = new Color( 0, 0, 0, 0 );

			// play death sound
			_audioSource.Play();
			yield return new WaitForSeconds( _audioSource.clip.length );

			//if the object is out of health, remove it from the game
			Destroy(this.gameObject);
		}
	}

	void StepBackwards()
	{
		Vector2 tPosition = transform.position;
		tPosition.x += _directionVector.x * -0.1f;
		tPosition.y += _directionVector.y * -0.1f;
		transform.position = tPosition;
	}

}
