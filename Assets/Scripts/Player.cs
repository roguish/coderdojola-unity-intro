﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	//class variables:
	//	public variables are visible in the Unity inspector and additional classes
	//	put [HideInInspector] above a public variable if you don't want it to be visible
	//	private variables are hidden.
	public float playerRotationSpeed;
	public int playerHealth;
	public GameObject bulletType;
	public float bulletLaunchPositionOffset;
	
	private float lastBulletShot;
	private Text _livesDisplay;
	private SceneSwitcher _sceneSwitcher;
	private Canvas _canvas;
	private BulletSuperclass _bulletScript;
	private AudioSource _audioInjury;
	private bool _isDying = false;

	// Use this for initialization
	void Start () {
		ResetBulletDelayTimer();

		_sceneSwitcher = GetComponent<SceneSwitcher>();

		_canvas = GameObject.Find("GameSceneCanvas").GetComponent<Canvas>();

		_livesDisplay = _canvas.transform.Find( "Lives Value" ).GetComponent<Text>(); 
		_livesDisplay.text = playerHealth.ToString();

		_bulletScript = bulletType.GetComponent<BulletSuperclass>();

		_audioInjury = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		if( _isDying ) return; 

		//Here we check for certain player input.
		//	If they press the left arrow, they will rotate around the Z axis.
		//	If they press the right arrow, we rotate them the opposite direction around the Z.
		if (Input.GetKey (KeyCode.LeftArrow)) {
			//Each game object in Unity has a transform. This keeps track of the
			//	position, rotation and scale of that object. The transform has its
			//	own functions that the programmer can use to manipulate it, such as
			//	Rotate(x, y, z).
			transform.Rotate (0, 0, Time.deltaTime*playerRotationSpeed);
			//deltaTime is the speed at which the game is running.
			//We multiply the playerSpeed by deltaTime so that the player will move at
			//	a consistent speed, no matter how fast the game is running.
		} else if (Input.GetKey (KeyCode.RightArrow)) {
			transform.Rotate (0, 0, -Time.deltaTime*playerRotationSpeed);
		}

		//If the player hits space, we will instantiate(create and place) a bullet prefab.
		if (Input.GetKey (KeyCode.Space) && Time.time - lastBulletShot > _bulletScript.bulletFireDelaySeconds) {
			// Instaniate the bullet at the player's position and rotation
			// use bulletLaunchOffset to adjust the bullet position to be at the end of the turret 
			Vector2 tForward = transform.right;
			tForward.Normalize();
			Vector2 tBulletPosition = transform.position;
			tBulletPosition.x += tForward.x * bulletLaunchPositionOffset;
			tBulletPosition.y += tForward.y * bulletLaunchPositionOffset;

			GameObject.Instantiate(bulletType, tBulletPosition, transform.rotation);

			ResetBulletDelayTimer();
		}

	}

	//This function is called by the Unity engine when this object overlaps another.
	void OnCollisionEnter2D(Collision2D collidedWith) {
		if (collidedWith.gameObject.tag == "Enemy" && !_isDying) {
			// Destroy the obstacle that collided with the player.
			Destroy(collidedWith.gameObject);

			// Decrease user health and update lives display
			playerHealth --;
			_livesDisplay.text = playerHealth.ToString();

			// play death sound
			_audioInjury.Play();

			// If user has died, go to Game Over screen
			if( playerHealth <= 0 )
			{
				_isDying = true;
				StartCoroutine( DoDeath() );
			}
		}
	}

	IEnumerator DoDeath()
	{
		GetComponent<BoxCollider2D>().enabled = false;
		yield return new WaitForSeconds( _audioInjury.clip.length );
		
		_sceneSwitcher.SwitchToSceneNumber( 2 );
	}

	void ResetBulletDelayTimer() {
		lastBulletShot = Time.time;
	}
}
